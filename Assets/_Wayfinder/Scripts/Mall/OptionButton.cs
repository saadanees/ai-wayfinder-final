﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Pathfinding;

public class OptionButton : MonoBehaviour
{
    string storeName;
    string id;

    Button button;

    private void Start()
    {
        button = GetComponent<Button>();
        button.onClick.AddListener(InitiateAction);
    }

    public void GetOption(Options.Option option)
    {
        storeName = option.store_name.ToLower();
        id = option.store_number;
        this.transform.GetChild(0).GetComponent<Text>().text = storeName;
        this.gameObject.name = id;
    }

    public void GetOption(CategoryDetails categoryDetails)
    {
        storeName = categoryDetails.category_name.ToLower();
        id = categoryDetails.category_id.ToString();
        this.transform.GetChild(0).GetComponent<Text>().text = storeName;
        this.gameObject.name = id;
    }

    public void InitiateAction()
    {
       // TheBrainScript.Instance.MakeActiveConversation = true;

        Debug.Log("InitiateAction for: " +storeName);
        TheBrainScript.Instance.PerformAction(storeName);
    }

}
