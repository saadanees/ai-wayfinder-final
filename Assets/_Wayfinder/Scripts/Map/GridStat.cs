﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Pathfinding
{
    public class GridStat : MonoBehaviour
    {
        public int visited = -1;
        public int x = 0;
        public int y = 0;

        // Start is called before the first frame update
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {

        }

        private void OnTriggerEnter(Collider other)
        {
            // print(other);
            if (other.tag == "Avoidable")
            {
                Destroy(this.gameObject);
            }
        }

        private void OnCollisionEnter(Collision collision)
        {
            print("Collision " + collision.gameObject.name);
        }

        private void OnDestroy()
        {
           // Debug.Log(this.name + " was destroyed");
        }
    }
}
