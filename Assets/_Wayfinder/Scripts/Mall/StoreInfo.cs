﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Pathfinding
{
    public class StoreInfo : MonoBehaviour
    {
        PointOfInterest.StoreDetails StoreDetails;

        public Text storeName;
        public Text storeLocation;
        public Text storeTiming;
        public Text storeNumber;
        public Text storeDescription;

        TargetUI targetUI;
        // Start is called before the first frame update

        private void OnEnable()
        {
            targetUI = GameObject.FindObjectOfType<TargetUI>();
        }

        void Start()
        {
            if (TheBrainScript.Instance != null)
            {
                GetInfo();
            }
           
        }

        public void GetInfo()
        {
            if(TheBrainScript.Instance.state == TheBrainScript.State.Point_Of_Interest)
            {
                if (!TheBrainScript.Instance.directMap)
                {
                    GetComponent<MapController>().OpenInfo();
                    //print("store info");
                    storeName.text = MallData.Instance.StoreDetails.store_name;
                    storeLocation.text = MallData.Instance.StoreDetails.location;
                    storeTiming.text = (MallData.Instance.StoreDetails.time_from + " - " + MallData.Instance.StoreDetails.time_from);
                    storeNumber.text = MallData.Instance.StoreDetails.store_number;
                    storeDescription.text = MallData.Instance.StoreDetails.description;
                    foreach (var item in GameObject.FindObjectsOfType<Store>())
                    {
                        //print("dest" + item.ID);
                        if (item.Name == storeName.text)
                        {
                            print("dest: " + item.Name);
                            targetUI.Destination(item);
                            //testDest(item);
                        }

                    }
                    //print(MallData.Instance.Data.context.skills.mainskill.user_defined.webhook_result_1.result[0].store_details.owner);
                    // print("yo: " + MallData.Instance.storeDetails.owner);

                }
                else
                {
                    print("ASDAD");
                    TheBrainScript.Instance.PerformAction("hello");
                    TheBrainScript.Instance.directMap = false;
                }
            }
            else if(TheBrainScript.Instance.state == TheBrainScript.State.Find_Point_Of_Interest)
            {
                if (!TheBrainScript.Instance.directMap)
                {
                    GetComponent<MapController>().OpenInfo();
                    //print("store info");
                    storeName.text = MallData.Instance.InterestDetails.store_name;
                    storeLocation.text = MallData.Instance.InterestDetails.location;
                    storeTiming.text = (MallData.Instance.InterestDetails.time_from + " - " + MallData.Instance.InterestDetails.time_from);
                    storeNumber.text = MallData.Instance.InterestDetails.store_number;
                    storeDescription.text = MallData.Instance.InterestDetails.description;
                    foreach (var item in GameObject.FindObjectsOfType<Store>())
                    {
                        //print("dest" + item.ID);
                        if (item.Name == storeName.text)
                        {
                            print("dest: " + item.Name);
                            item.isActive = true;
                            targetUI.Destination(item);
                            //testDest(item);
                        }

                    }
                    //print(MallData.Instance.Data.context.skills.mainskill.user_defined.webhook_result_1.result[0].store_details.owner);
                    // print("yo: " + MallData.Instance.storeDetails.owner);

                }
                else
                {
                    print("ASDAD");
                    TheBrainScript.Instance.PerformAction("hello");
                    TheBrainScript.Instance.directMap = false;
                }
            }
           



        }


        //void testDest(Store store)
        //{
        //    StartCoroutine(Tests(store));
        //}

        //IEnumerator Tests(Store s)
        //{
        //    yield return new WaitForSeconds(0.5f);
        //    GameObject.FindObjectOfType<TargetUI>().Destination(s);
        //}

        // Update is called once per frame
        void Update()
        {
          
        }
    }
}
