﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace Pathfinding
{
    public class TargetUI : MonoBehaviour
    {
        private static TargetUI instance = null;

        public static TargetUI Instance
        {
            get
            {
                return instance;
            }
        }

        //public Transform nikeStore1;
        //public Transform nikeStore2;
        //public Transform hmStore;
        //public Transform foodStore;
        //public Transform cinemaStore;
        //public Transform centerPointStore;
        //public Transform washroom1;
        //public Transform baskinRobbinsStore;
        public TargetMover targetMover;
        public bool next;
        Transform nextDestination;

        RaycastHit hit;
        AIDestinationSetter _AIDestinationSetter;
        private void Awake()
        {
            // if the singleton hasn't been initialized yet
            if (instance != null && instance != this)
            {
                Destroy(this.gameObject);
            }
            _AIDestinationSetter = GameObject.FindObjectOfType<AIDestinationSetter>();
            //print(_AIDestinationSetter.target);
            instance = this;
            DontDestroyOnLoad(this.gameObject);
        }

        // Start is called before the first frame update
        void Start()
        {
            nextDestination = this.transform;
            next = false;
            //print(stores[0].ID);
        }

        // Update is called once per frame
        void Update()
        {
            if(next)
            {
                this.transform.position = new Vector3(nextDestination.localPosition.x, nextDestination.localPosition.y, nextDestination.localPosition.z);

                _AIDestinationSetter.target = this.transform; // jugaar
                                                              // print(_AIDestinationSetter.target.position);
                next = false;
            }
          
            //if(Input.GetKey(KeyCode.A))
            //{
            //    Destination(001);
            //}



            if (Input.GetMouseButtonUp(0) && TheBrainScript.Instance.activeConversation)
            {
               // print("mouse pos: " + Input.mousePosition);
                Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                if (Physics.Raycast(ray, out hit))
                {
                    if (hit.collider.tag == "Clickable")
                    {
                        
                        var storeInfo = hit.collider.gameObject.GetComponent<Store>();
                        if(TheBrainScript.Instance)
                            TheBrainScript.Instance.PerformAction(storeInfo.Name);
                        Destination(storeInfo);
                        
                    }
                 
                }
            }
        }



        public void Destination(Store _store)
        {
            print("Next Destination " + _store.CurrentLoc());
            //print("HERE DEST " + _store.Location.position);
            nextDestination = _store.Location;
           // print(nextDestination.localPosition);
            //nextDestination.position = new Vector3(_store.CurrentLoc().x, _store.CurrentLoc().y, _store.CurrentLoc().z);

            next = true;
            _AIDestinationSetter.canCalculatePath = true;

            targetMover.UpdateTargetPosition(nextDestination.position);
            //if(SceneManager.GetActiveScene().name == "Map")
            //  GameObject.FindObjectOfType<StoreInfo>().GetInfo();

        }
    }
}


