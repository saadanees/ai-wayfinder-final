﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Pathfinding;

public class OptionsPanel : MonoBehaviour
{
    public GameObject optionBtnPrefab;
    public Transform container;
    string startingText;

   public void Populate(Options.Option option)
    {
        GameObject btn = Instantiate(optionBtnPrefab);
       
        btn.transform.SetParent(container, false);
        btn.AddComponent<OptionButton>().GetOption(option);
   
    }

    public void Populate(CategoryDetails categoryDetails)
    {
        GameObject btn = Instantiate(optionBtnPrefab);

        btn.transform.SetParent(container, false);
        btn.AddComponent<OptionButton>().GetOption(categoryDetails);

    }
}
