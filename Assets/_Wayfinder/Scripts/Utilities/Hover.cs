﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hover : MonoBehaviour {

public float horizontalSpeed;
public float verticalSpeed;
public float Amplitude;

Vector3 tempPos;
	// Use this for initialization
	void Start () {
		tempPos = transform.position;
		//print(tempPos.y);
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		tempPos.x += horizontalSpeed;
		tempPos.y = 2.0f + Mathf.Sin(Time.realtimeSinceStartup * verticalSpeed) * Amplitude;
		transform.position = tempPos;
	}
}
