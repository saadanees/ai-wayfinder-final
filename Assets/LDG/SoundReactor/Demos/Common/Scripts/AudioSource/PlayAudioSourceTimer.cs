﻿using UnityEngine;

public class PlayAudioSourceTimer : MonoBehaviour
{
    public float timer = 1;

    private AudioSource audioSource;

    // Use this for initialization
    void Start ()
    {
        audioSource = GetComponent<AudioSource>();
        audioSource.PlayDelayed(timer);
	}

    public void Pause()
    {
        audioSource.Pause();
    }

    public void UnPause()
    {
        audioSource.UnPause();
    }

    public void Play()
    {
        audioSource.PlayDelayed(timer);
    }

    public void Stop()
    {
        audioSource.Stop();
    }

    public void PlayDelayed(float delay)
    {

    }
}
