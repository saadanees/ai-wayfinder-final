﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FacilityBoard : MonoBehaviour
{
    private void LateUpdate()
    {
        //transform.forward = new Vector3(Camera.main.transform.forward.x, transform.forward.y, Camera.main.transform.forward.z);
        if (!MapController.Instance.MapToggle)
        {
            //isometric
            gameObject.GetComponent<SpriteRenderer>().enabled = false;
           
        }
        else
        {
            gameObject.GetComponent<SpriteRenderer>().enabled = true;
        }
    }
}
