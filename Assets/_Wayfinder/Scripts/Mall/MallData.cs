﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

namespace Pathfinding
{

    public class MallData : MonoBehaviour
    {
        private static MallData instance = null;


        public static MallData Instance
        {
            get
            {
                return instance;
            }
        }

        public PointOfInterest.StoreDataRoot Data;
        public PointOfInterest.StoreDetails StoreDetails;

        public FindPointOfInterest.StoreDetails InterestDetails;

        public Command.CommandData CommandData;

        public Options.OptionsData OptionsData;

        public FindPointOfInterest.Root InterestRoot;
        public Categories Categories;

        private void Awake()
        {
            // if the singleton hasn't been initialized yet
            if (instance != null && instance != this)
            {
                Destroy(this.gameObject);
            }

            instance = this;
            DontDestroyOnLoad(this.gameObject);
        }

    }




    // Root myDeserializedClass = JsonConvert.DeserializeObject<Root>(myJsonResponse); 
    [Serializable]
    public class PointOfInterest
    {
        [Serializable]
        public class Entity
        {
            public string entity;
            public List<int> location;
            public string value;
            public double confidence;

        }

        [Serializable]
        public class Generic
        {
            public string response_type;
            public string text;

        }

        [Serializable]
        public class Output
        {
            public List<object> intents;
            public List<Entity> entities;
            public List<object> actions;
            public List<Generic> generic;

        }

        [Serializable]
        public class System
        {
            public int turn_count;

        }

        [Serializable]
        public class Global
        {
            public System system;
            public string session_id;

        }

        [Serializable]
        public class Result
        {
            public string message;

        }

        [Serializable]
        public class TriggerResult
        {
            public Result result;

        }

        [Serializable]
        public class StoreDetails
        {
            public int access_id;
            public string contact_number;
            public string day_from;
            public string day_to;
            public string description;
            public string email;
            public string location;
            public string owner;
            public int status;
            public string store_name;
            public string store_number;
            public List<object> store_sysnonyms;
            public string time_from;
            public string time_to;
            public string user_name;

        }

        [Serializable]
        public class Result2
        {
            public int response;
            public string store_category;
            public int store_category_id;
            public StoreDetails store_details;
            public List<object> store_promotions;

        }

        [Serializable]
        public class WebhookResult1
        {
            public List<Result2> result;

        }

        [Serializable]
        public class UserDefined
        {
            public string trigger;
            public TriggerResult trigger_result;
            public string storeNumber;
            public WebhookResult1 webhook_result_1;

        }


        [Serializable]
        public class System2
        {

        }

        [Serializable]
        public class Mainskill
        {
            public UserDefined user_defined;
            public System2 system;

        }

        [Serializable]
        public class Skills
        {
            public Mainskill mainskill;

        }

        [Serializable]
        public class Context
        {
            public Global global;
            public Skills skills;

        }

        [Serializable]
        public class StoreDataRoot
        {
            public Output output;
            public Context context;

        }


    }

    [Serializable]
    public class Command
    {
        [Serializable]
        public class Intent
        {
            public string intent;
            public double confidence;

        }

        [Serializable]
        public class Entity
        {
            public string entity;
            public List<int> location;
            public string value;
            public int confidence;

        }

        [Serializable]
        public class Generic
        {
            public string response_type;
            public string text;

        }

        [Serializable]
        public class Output
        {
            public List<Intent> intents;
            public List<Entity> entities;
            public List<object> actions;
            public List<Generic> generic;

        }

        [Serializable]
        public class System
        {
            public int turn_count;

        }

        [Serializable]
        public class Global
        {
            public System system;
            public string session_id;

        }

        [Serializable]
        public class Result
        {
            public string message;

        }

        [Serializable]
        public class CommandResult
        {
            public Result result;

        }

        [Serializable]
        public class UserDefined
        {
            public string command;
            public CommandResult command_result;

        }

        [Serializable]
        public class System2
        {

        }

        [Serializable]
        public class Mainskill
        {
            public UserDefined user_defined;
            public System2 system;

        }

        [Serializable]
        public class Skills
        {
            public Mainskill mainskill;

        }

        [Serializable]
        public class Context
        {
            public Global global;
            public Skills skills;

        }

        [Serializable]
        public class CommandData
        {
            public Output output;
            public Context context;

        }
    }

    [Serializable]
    public class Options
    {
        [Serializable]
        public class Intent
        {
            public string intent;
            public double confidence;

        }

        [Serializable]
        public class Entity
        {
            public string entity;
            public List<int> location;
            public string value;
            public int confidence;

        }

        [Serializable]
        public class Generic
        {
            public string response_type;
            public string text;

        }

        [Serializable]
        public class Output
        {
            public List<Intent> intents;
            public List<Entity> entities;
            public List<object> actions;
            public List<Generic> generic;

        }

        [Serializable]
        public class System
        {
            public int turn_count;

        }

        [Serializable]
        public class Global
        {
            public System system;
            public string session_id;

        }

        [Serializable]
        public class Option
        {
            public string store_name;
            public string store_number;

        }

        [Serializable]
        public class Result
        {
            public string message;
            public List<Option> options;
        }

        [Serializable]
        public class OptionList
        {
            public List<Result> result;
           

        }

        [Serializable]
        public class UserDefined
        {
            public string options;
            public OptionList option_list;

        }

        [Serializable]
        public class System2
        {

        }

        [Serializable]
        public class Mainskill
        {
            public UserDefined user_defined;
            public System2 system;

        }

        [Serializable]
        public class Skills
        {
            public Mainskill mainskill;

        }

        [Serializable]
        public class Context
        {
            public Global global;
            public Skills skills;

        }

        [Serializable]
        public class OptionsData
        {
            public Output output;
            public Context context;

        }



    }

    [Serializable]
    public class FindPointOfInterest
    {
        [Serializable]
        public class Intent
        {
            public string intent;
            public double confidence;

        }

        [Serializable]
        public class Entity
        {
            public string entity;
            public List<int> location;
            public string value;
            public int confidence;

        }

        [Serializable]
        public class Generic
        {
            public string response_type;
            public string text;

        }

        [Serializable]
        public class Output
        {
            public List<Intent> intents;
            public List<Entity> entities;
            public List<object> actions;
            public List<Generic> generic;

        }

        [Serializable]
        public class System
        {
            public int turn_count;

        }

        [Serializable]
        public class Global
        {
            public System system;
            public string session_id;

        }

        [Serializable]
        public class Result
        {
            public string message;

        }

        [Serializable]
        public class TriggerResult
        {
            public Result result;

        }

        [Serializable]
        public class StoreDetails
        {
            public int access_id;
            public string contact_number;
            public string day_from;
            public string day_to;
            public string description;
            public string email;
            public string location;
            public string owner;
            public int status;
            public string store_name;
            public string store_number;
            public string time_from;
            public string time_to;
            public string user_name;

        }

        [Serializable]
        public class StoreSysnonym
        {
            public List<string> synonyms;

        }

        [Serializable]
        public class Result2
        {
            public int response;
            public string store_category;
            public int store_category_id;
            public StoreDetails store_details;
            public List<object> store_promotions;
            public List<StoreSysnonym> store_sysnonyms;

        }

        [Serializable]
        public class WebhookResult1
        {
            public List<Result2> result;

        }

        [Serializable]
        public class UserDefined
        {
            public string trigger;
            public TriggerResult trigger_result;
            public string storeNumber;
            public WebhookResult1 webhook_result_1;

        }

        [Serializable]
        public class System2
        {

        }

        [Serializable]
        public class Mainskill
        {
            public UserDefined user_defined;
            public System2 system;

        }

        [Serializable]
        public class Skills
        {
            public Mainskill mainskill;

        }

        [Serializable]
        public class Context
        {
            public Global global;
            public Skills skills;

        }

        [Serializable]
        public class Root
        {
            public Output output;
            public Context context;

        }


    }

    /// <summary>
    /// GET Category 
    /// </summary>\
    /// 
    [Serializable]
    public class CategoryShortNames
    {
        public List<string> synonyms;

    }

    [Serializable]
    public class CategoryDetails
    {
        public string category_description;
        public int category_id;
        public string category_name;
        public CategoryShortNames category_short_names;

    }

    [Serializable]
    public class Categories
    {
        public CategoryDetails[] details;

    }
}
