﻿#pragma warning disable 0649

using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using IBM.Watson.SpeechToText.V1;
using IBM.Cloud.SDK;
using IBM.Cloud.SDK.Utilities;
using IBM.Cloud.SDK.DataTypes;
using IBM.Watson.Assistant.V2;
using IBM.Watson.Assistant.V2.Model;
using IBM.Watson.TextToSpeech.V1;
using IBM.Watson.TextToSpeech.V1.Model;
using System.Linq;
using System;
using IBM.Cloud.SDK.Authentication;
using IBM.Cloud.SDK.Authentication.Iam;
using UnityEngine.SceneManagement;
using Pathfinding;
using Newtonsoft.Json;
using UnityEngine.Networking;
using IBM.Watson.SpeechToText.V1.Model;

public class TheBrainScript : MonoBehaviour
{
    private static TheBrainScript instance = null;


    public static TheBrainScript Instance
    {
        get
        {
            return instance;
        }
    }

    private void Awake()
    {
        // if the singleton hasn't been initialized yet
        if (instance != null && instance != this)
        {
            Destroy(this.gameObject);
        }

        instance = this;
        DontDestroyOnLoad(this.gameObject);
    }

    private const string CategoryURL = "http://wayfinder.pixelplusmedia.com:5000/Category";

    #region SPEECH TO TEXT (STT) SETTING (PLEASE SET THESE VARIABLES IN THE INSPECTOR)
    [Space(10)]
    [Tooltip("STT service URL (optional). This defaults to \"https://stream.watsonplatform.net/speech-to-text/api\"")]

    public Text TTSText;
    [Tooltip("Text field to display the results of streaming.")]
    public Text STTText;
    [SerializeField]
    private string _STTserviceUrl;
    [Header("IAM Authentication")]
    [Tooltip("The IAM apikey.")]
    [SerializeField]
    private string _iamApikeySTT;

    [Header("Parameters")]
    // https://www.ibm.com/watson/developercloud/speech-to-text/api/v1/curl.html?curl#get-model
    [Tooltip("The Model to use (optional). This defaults to en-US_BroadbandModel")]
    [SerializeField]
    private string _recognizeModel;
    #endregion

    #region SPEECH TO TEXT (STT) GLOBAL VARIABLES
    private int _recordingRoutine = 0;
    private string _microphoneID = null;
    private AudioClip _recording = null;
    private int _recordingBufferSize = 1;
    private int _recordingHZ = 22050;

    private SpeechToTextService _speechtotextService;
    #endregion

    #region ASSISTANT (AST) SETTING PLEASE SET THESE VARIABLES IN THE INSPECTOR
    [Space(10)]
    [Tooltip("Assistant IAM apikey.")]
    [SerializeField]
    private string _iamApikeyAST;
    [Tooltip("Assistant service URL (optional). This defaults to \"https://gateway.watsonplatform.net/assistant/api\"")]
    [SerializeField]
    private string _ASTserviceUrl;
    [Tooltip("The version date with which you would like to use the service in the form YYYY-MM-DD (2019-02-18).")]
    [SerializeField]
    private string versionDate;
    [Tooltip("The assistantId to run the example.")]
    [SerializeField]
    private string assistantId;
    #endregion

    #region ASSISTANT GLOBAL VARIABLES
    private AssistantService _assistant;

    private bool createSessionTested = false;
    private bool messageTested0 = false;

    private string sessionId;
    private string synthesizeText = null;

    #endregion

    #region TEXT TO SPEECH (TTS) PLEASE SET THESE VARIABLES IN THE INSPECTOR
    [Space(10)]
    [Tooltip("TTS service URL (optional). This defaults to \"https://stream.watsonplatform.net/speech-to-text/api\"")]
    [SerializeField]
    private string _TTSserviceUrl;
    [Header("TTS IAM Authentication")]
    [Tooltip("TTS The IAM apikey.")]
    [SerializeField]
    private string _iamApikeyTTS;

    private string allisionVoice = "en-US_MichaelV3Voice";// en-US_AllisonVoice
    private string synthesizeMimeType = "audio/wav";
    private string voiceModelName = "unity-sdk-voice-model";
    private string voiceModelNameUpdated = "unity-sdk-voice-model-updated";
    private string voiceModelLanguage = "en-US";
    private string customizationId;

    private TextToSpeechService _texttospeech;
    #endregion

    private AudioClip audioClip;
    [SerializeField]
    private AudioSource audioSource;

    private Transform currSpectrumTransform;
    float seconds;
    bool IsMapLoaded = false;

    private string[] botName = { "hello", "hallow", "halo", "hellow" };

    [SerializeField]
    public bool activeConversation = false;
    public Camera cam;
    public GameObject SpectrumGo;
    public float spectrumX;
    public float spectrumY;
    public float spectrumWidth;
    public float spectrumHeight;
    public bool directMap;
    public GameObject OptionsPanelGo;
    public GameObject SearchPanelGO;

    public enum State
    {
        Idle,
        Listening,
        Speaking,
        Working,
        Trigger,
        Command,
        Options,
        Point_Of_Interest,
        Nearest_Point_Of_Interest,
        Find_Point_Of_Interest

    }

    public State state;

    void Start()
    {
        directMap = false;
        currSpectrumTransform = SpectrumGo.transform;
        audioSource = GetComponent<AudioSource>();
        state = State.Idle;
        //LogSystem.InstallDefaultReactors();
        Runnable.Run(STTCreateService());
        Runnable.Run(ASTCreateService());
        Runnable.Run(TTSCreateService());

        //StartCoroutine(GetCateories(CategoryURL));
    }

    #region STT SERVICE INSTANTIATE
    private IEnumerator STTCreateService()
    {
        if (string.IsNullOrEmpty(_iamApikeySTT))
        {
            throw new IBMException("Plesae provide IAM ApiKey for the service.");
        }

        IamAuthenticator authenticator = new IamAuthenticator(apikey: _iamApikeySTT);

        //  Wait for tokendata
        while (!authenticator.CanAuthenticate())
            yield return null;



        _speechtotextService = new SpeechToTextService(authenticator);
        if (!string.IsNullOrEmpty(_STTserviceUrl))
        {
            _speechtotextService.SetServiceUrl(_STTserviceUrl);
        }
        _speechtotextService.StreamMultipart = true;

        ActiveSTT = true;
        StartRecording();
    }
    #endregion

    #region SST ACTIVE STATE
    public bool ActiveSTT
    {
        get { return _speechtotextService.IsListening; }
        set
        {
            if (value && !_speechtotextService.IsListening)
            {
                _speechtotextService.RecognizeModel = (string.IsNullOrEmpty(_recognizeModel) ? "en-US_BroadbandModel" : _recognizeModel);
                _speechtotextService.DetectSilence = true;
                _speechtotextService.EnableWordConfidence = true;
                _speechtotextService.EnableTimestamps = true;
                _speechtotextService.SilenceThreshold = 0.01f;
                _speechtotextService.MaxAlternatives = 1;
                _speechtotextService.EnableInterimResults = true;
                _speechtotextService.OnError = OnError;
                _speechtotextService.InactivityTimeout = -1;
                _speechtotextService.ProfanityFilter = false;
                _speechtotextService.SmartFormatting = true;
                _speechtotextService.SpeakerLabels = false;
                _speechtotextService.WordAlternativesThreshold = null;
                SpeechToTextService.OnRecognize onRecognize = OnRecognize;
                _speechtotextService.StartListening(onRecognize, OnRecognizeSpeaker);
            }
            else if (!value && _speechtotextService.IsListening)
            {
                _speechtotextService.StopListening();
            }
        }
    }
    #endregion

    #region STT MODELS AND FUNCTIONS
    private void StartRecording()
    {
        if (_recordingRoutine == 0)
        {
            UnityObjectUtil.StartDestroyQueue();
            _recordingRoutine = Runnable.Run(RecordingHandler());
        }
        // print("Recording");
        //state = State.Listening;
    }

    private void StopRecording()
    {
        if (_recordingRoutine != 0)
        {
            Microphone.End(_microphoneID);
            Runnable.Stop(_recordingRoutine);
            _recordingRoutine = 0;
            //Listen();
        }
        //  print("StopRecording");
    }

    private void OnError(string error)
    {
        ActiveSTT = false;

        //Log.Debug("ExampleStreaming.OnError()", "Error! {0}", error);
    }

    private IEnumerator RecordingHandler()
    {
        //Log.Debug("ExampleStreaming.RecordingHandler()", "devices: {0}", Microphone.devices);
        //print(Microphone.devices.Length + " " + Microphone.devices[0]);
        _microphoneID = Microphone.devices[0];
        _recording = Microphone.Start(_microphoneID, true, _recordingBufferSize, _recordingHZ);
        //_recording = Microphone.Start(Microphone.devices[0], true, _recordingBufferSize, _recordingHZ);
        yield return null;      // let _recordingRoutine get set..

        if (_recording == null)
        {
            StopRecording();
            yield break;
        }

        bool bFirstBlock = true;
        int midPoint = _recording.samples / 2;
        float[] samples = null;

        while (_recordingRoutine != 0 && _recording != null)
        {
            int writePos = Microphone.GetPosition(_microphoneID);
            if (writePos > _recording.samples || !Microphone.IsRecording(_microphoneID))
            {
                Log.Error("ExampleStreaming.RecordingHandler()", "Microphone disconnected.");

                StopRecording();
                yield break;
            }

            if ((bFirstBlock && writePos >= midPoint)
              || (!bFirstBlock && writePos < midPoint))
            {
                // front block is recorded, make a RecordClip and pass it onto our callback.
                samples = new float[midPoint];
                _recording.GetData(samples, bFirstBlock ? 0 : midPoint);

                AudioData record = new AudioData();
                record.MaxLevel = Mathf.Max(Mathf.Abs(Mathf.Min(samples)), Mathf.Max(samples));
                record.Clip = AudioClip.Create("Recording", midPoint, _recording.channels, _recordingHZ, false);
                record.Clip.SetData(samples, 0);

                _speechtotextService.OnListen(record);

                bFirstBlock = !bFirstBlock;
            }
            else
            {
                // calculate the number of samples remaining until we ready for a block of audio, 
                // and wait that amount of time it will take to record.
                int remaining = bFirstBlock ? (midPoint - writePos) : (_recording.samples - writePos);
                float timeRemaining = (float)remaining / (float)_recordingHZ;

                yield return new WaitForSeconds(timeRemaining);
            }
        }
        yield break;
    }

    private void OnRecognize(SpeechRecognitionEvent result)
    {
        if (result != null && result.results.Length > 0)
        {
            foreach (var res in result.results)
            {
                foreach (var alt in res.alternatives)
                {
                    string text = string.Format("{0} ({1}, {2:0.00})\n", alt.transcript, res.final ? "Final" : "Interim", alt.confidence);
                    string clearText = alt.transcript.TrimStart().TrimEnd();
                    //Log.Debug("ExampleStreaming.OnRecognize()", text);

                    STTText.text = clearText;
                    double conf = alt.confidence;

                    //print(res.final);

                    // On result send AI Message
                    //Runnable.Run(AISendMessage(text));
                    string[] words = alt.transcript.Split(' ');
                    //bool amICalled = false;
                    foreach (var word in words)
                    {
                        if(botName.Any(x => x == word.Trim()))
                        {
                            activeConversation = botName.Any(x => x == word.Trim());
                            print("Activated : " + activeConversation);
                        }
                           


                       
                    }

                    if (activeConversation && res.final && clearText != null)
                    {
                        string _conversationString = alt.transcript;
                        //We can now call the CONV service?
                        //Log.Debug("STT.OnSTTRecognize()", _conversationString);

                        ActiveSTT = false;  //Stop Microphone from listening

                        var input_text = new MessageInput()
                        {
                            Text = _conversationString,
                            Options = new MessageInputOptions()
                            {
                                ReturnContext = true
                            }

                        };

                        _assistant.Message(OnMessageSendRequest, assistantId, sessionId, input: input_text);
                    }
                }

                //if (res.keywords_result != null && res.keywords_result.keyword != null)
                //{
                //    foreach (var keyword in res.keywords_result.keyword)
                //    {
                //        Log.Debug("ExampleStreaming.OnRecognize()", "keyword: {0}, confidence: {1}, start time: {2}, end time: {3}", keyword.normalized_text, keyword.confidence, keyword.start_time, keyword.end_time);
                //    }
                //}

                //if (res.word_alternatives != null)
                //{
                //    foreach (var wordAlternative in res.word_alternatives)
                //    {
                //        Log.Debug("ExampleStreaming.OnRecognize()", "Word alternatives found. Start time: {0} | EndTime: {1}", wordAlternative.start_time, wordAlternative.end_time);
                //        foreach (var alternative in wordAlternative.alternatives)
                //            Log.Debug("ExampleStreaming.OnRecognize()", "\t word: {0} | confidence: {1}", alternative.word, alternative.confidence);
                //    }
                //}
            }
        }
    }

    // load map
    private IEnumerator ActivateMap(bool canLoadMap)
    {
        yield return new WaitForSeconds(audioClip.length);
        if(canLoadMap)
        {
            if (IsMapLoaded)
            {
                SceneManager.LoadSceneAsync(1, LoadSceneMode.Additive);
                SpectrumGo.transform.position = new Vector3(
                    SpectrumGo.transform.position.x + spectrumX,
                    SpectrumGo.transform.position.y + spectrumY,
                    SpectrumGo.transform.position.z);

                SpectrumGo.transform.localScale = new Vector3(spectrumWidth, spectrumHeight, 1f);
                SpectrumGo.transform.GetChild(0).gameObject.SetActive(false);

                SearchPanelGO.SetActive(false);
                OptionsPanelGo.SetActive(false);
               

            }
        }
        else
        {
            SpectrumGo.transform.position = new Vector3(0, 0, 0);
            SpectrumGo.transform.localScale = new Vector3(1f, 1f, 1f);
            SpectrumGo.transform.GetChild(0).gameObject.SetActive(true);
            SceneManager.UnloadSceneAsync(1);
            state = State.Listening;
            //_assistant.Message(OnMessageInitiate, assistantId, sessionId);
        }
        

    }

    private void OnRecognizeSpeaker(SpeakerRecognitionEvent result)
    {
        if (result != null)
        {
            //foreach (SpeakerLabelsResult labelResult in result.speaker_labels)
            //{
            //    //Log.Debug("ExampleStreaming.OnRecognizeSpeaker()", string.Format("speaker result: {0} | confidence: {3} | from: {1} | to: {2}", labelResult.speaker, labelResult.from, labelResult.to, labelResult.confidence));
            //}
        }
    }
    #endregion

    #region AST SERVICE INSTANTIATE
    private IEnumerator ASTCreateService()
    {
        if (string.IsNullOrEmpty(_iamApikeyAST))
        {
            throw new IBMException("Plesae provide IAM ApiKey for the service.");
        }

        //  Create credential and instantiate service
        IamAuthenticator authenticator = new IamAuthenticator(apikey: _iamApikeyAST);

        //  Wait for tokendata
        while (!authenticator.CanAuthenticate())
            yield return null;

        _assistant = new AssistantService(versionDate, authenticator);
        if (!string.IsNullOrEmpty(_ASTserviceUrl))
        {
            _assistant.SetServiceUrl(_ASTserviceUrl);
        }


        Runnable.Run(AISendInitateSession());
    }
    #endregion

    #region AST MODELS AND FUNCTIONS
    private IEnumerator AISendInitateSession()
    {
        //Log.Debug("ExampleAssistantV2.RunTest()", "Attempting to CreateSession");
        _assistant.CreateSession(OnCreateSession, assistantId);
        
        while (!createSessionTested)
        {
            yield return null;
        }

        //Log.Debug("ExampleAssistantV2.RunTest()", "Attempting to Message");
        _assistant.Message(OnMessageInitiate, assistantId, sessionId);

        while (!messageTested0)
        {
            yield return null;
        }

    }

    private void OnDeleteSession(DetailedResponse<object> response, IBMError error)
    {
        print("session Deleted");
        createSessionTested = false;
        Log.Debug("AssistantV2.OnDeleteSession()", "Session deleted.");
        //deleteSessionTested = true;
    }

    private void OnCreateSession(DetailedResponse<SessionResponse> response, IBMError error)
    {
        //Log.Debug("AssistantV2.OnCreateSession()", "Session: {0}", response.Result.SessionId);
       
        sessionId = response.Result.SessionId;
        createSessionTested = true;
        Debug.Log("Session ID Creadted: " + sessionId);
    }


    private void OnMessageInitiate(DetailedResponse<MessageResponse> response, IBMError error)
    {
        StopRecording();
        try
        {
            //Log.Debug("AssistantV2.OnMessage1()", "response: {0}", response.Result.Output.Generic[0].Text);

            synthesizeText = response.Result.Output.Generic[0].Text;
            state = State.Listening;
            Debug.Log("OnMessageInitiate " + response.Result.Output.Generic[0].Text);
        }
        catch
        {
            //Log.Debug("AssistantV2.OnMessage1()", "response: {0}", "My master did not teach me about that");
            synthesizeText = response.Result.Output.Generic[0].Text;
            print("Catch: " + response.Result.Output.Generic[0].Text);
        }
        // print("OnMessageInitiate");
        Runnable.Run(Synthesize());
        messageTested0 = true;
    }

    private void OnMessageSendRequest(DetailedResponse<MessageResponse> response, IBMError error)
    {
        StopRecording();
        try
        {


            string json = response.Response.Replace("main skill", "mainskill"); // to remove space to ind class

            Debug.Log("OnMessageSendRequest Response: " + json);
            //print(response.Result.Context.Skills
           
            if (response.Result.Output.Intents.Any())
            {
                if (response.Result.Output.Intents[0].Intent == "trigger" && (state == State.Listening || state == State.Idle))
                {
                   
                   
                    print("Its a trigger");
                    activeConversation = true;

                    if(!SceneManager.GetSceneByName("Map").isLoaded)
                    {
                        state = State.Trigger;
                        //SearchPanelGO.SetActive(true);
                        InitiateCategory();
                    }
                    
                   
                }
                else if (activeConversation)
                {
                    Debug.Log("it's a " +response.Result.Output.Intents[0].Intent);
                    switch (response.Result.Output.Intents[0].Intent)
                    {
                        case "trigger":
                            //print("trigger");
                            state = State.Trigger;
                            activeConversation = true;
                            break;

                        case "command":
                            //print("command");
                            state = State.Command;
                            MallData.Instance.CommandData = JsonConvert.DeserializeObject<Command.CommandData>(json);
                            var command = MallData.Instance.CommandData;
                            PerformAction(command);
                            break;

                        case "options":
                            //print("options");
                            state = State.Options;
                            MallData.Instance.OptionsData = JsonConvert.DeserializeObject<Options.OptionsData>(json);
                            var options = MallData.Instance.OptionsData;
                            PerformAction(options);
                            break;

                        case "find_point_of_interest":
                            //print("find_point_of_interest");
                            state = State.Find_Point_Of_Interest;
                            
                            MallData.Instance.InterestRoot = JsonConvert.DeserializeObject<FindPointOfInterest.Root>(json);
                            MallData.Instance.InterestDetails = MallData.Instance.InterestRoot.context.skills.mainskill.user_defined.webhook_result_1.result[0].store_details;

                            var interest = MallData.Instance.InterestRoot;

                            PerformAction(interest);
                            break;

                        default:
                            print("I dont know");
                            break;
                    }
                  
                }
                else
                {
                    print("after here");
                }
                

            }
            else if (response.Result.Output.Entities.Any())
            {
                if (!activeConversation)
                {
                    // if not activated
                    print("Whats that");
                    synthesizeText = "A.I is not activated.";
                    Runnable.Run(Synthesize());
                    return;
                }

                switch (response.Result.Output.Entities[0].Entity)
                {
                    case "point_of_interest":
                        print("point_of_interest");
                        state = State.Point_Of_Interest;
                        MallData.Instance.Data = JsonConvert.DeserializeObject<PointOfInterest.StoreDataRoot>(json);
                        MallData.Instance.StoreDetails = MallData.Instance.Data.context.skills.mainskill.user_defined.webhook_result_1.result[0].store_details;
                        var storeDataroot = MallData.Instance.Data;
                        PerformAction(storeDataroot);
                       // activeConversation = false;
                        break;


                    default:
                        print("I dont know");
                        break;
                }
            }
            //print("Text: " + response.Result.Output.Generic[0].Text);
            synthesizeText = response.Result.Output.Generic[0].Text;
            //DoAction(json);

            Debug.Log("OnMessageSendRequest Completed: " + response.Result.Output.Generic[0].Text);

        }
        catch(Exception e)
        {

            //Log.Debug("AssistantV2.OnMessage1()", "response: {0}", "My master did not teach me about that");
            Debug.Log("Catch Error: " + e.Message);
            StartCoroutine(RestartSession());
            synthesizeText = "Oh! Session Expird";
            //synthesizeText = response.Result.Output.Generic[0].Text;
        }
        //print("OnMessageSendRequest");
        Runnable.Run(Synthesize());

     
    }
    #endregion

    IEnumerator RestartSession()
    {
        _assistant.DeleteSession(OnDeleteSession, assistantId, sessionId);
        while (createSessionTested)
        {
            yield return null;
        }

        StartCoroutine(AISendInitateSession());
    }


    #region TTS SERVICE INSTATIATE
    private IEnumerator TTSCreateService()
    {
        if (string.IsNullOrEmpty(_iamApikeyTTS))
        {
            throw new IBMException("Plesae provide IAM ApiKey for the service.");
        }

        IamAuthenticator authenticator = new IamAuthenticator(apikey: _iamApikeyTTS);

        while (!authenticator.CanAuthenticate())
        {
            yield return null;
        }

        _texttospeech = new TextToSpeechService(authenticator);
        if (!string.IsNullOrEmpty(_TTSserviceUrl))
        {
            _texttospeech.SetServiceUrl(_TTSserviceUrl);
        }

        //_texttospeech = new TextToSpeechService(credentials);
        //Runnable.Run(TestSynthesize());
    }
    #endregion

    #region TTS Synthesize
    public IEnumerator Synthesize()
    {
        if (synthesizeText != null)
        {

            TTSText.text = synthesizeText;
            Log.Debug("TextToSpeechServiceV1IntegrationTests", "Attempting to Synthesize...");
            byte[] synthesizeResponse = null;
            AudioClip clip = null;
            _texttospeech.Synthesize(
                callback: (DetailedResponse<byte[]> response, IBMError error) =>
                {
                    // Show transcript in UI
                    synthesizeResponse = response.Result;
                    //print("clip it: " + synthesizeResponse);
                    clip = WaveFile.ParseWAV("myClip", synthesizeResponse);
                    audioClip = clip;
                    PlayClip(clip);
                },
                text: synthesizeText,
                voice: allisionVoice,
                accept: synthesizeMimeType

            );



            while (synthesizeResponse == null)
                yield return null;

            yield return new WaitForSeconds(clip.length);

            //activeConversation = false;
            TTSText.text = "";
            StartRecording(); // to start recording again

        }


    }
    #endregion

    #region TTS PlayClip
    private void PlayClip(AudioClip clip)
    {
        //if (Application.isPlaying && clip != null)
        //{

        audioSource.spatialBlend = 0.0f;
        audioSource.loop = false;
        audioSource.clip = clip;
        audioSource.Play();

        //GameObject.Destroy(salsaAudioSource, clip.length);

        synthesizeText = null;
        STTText.text = "";
        //TTSText.text = "";
        //if(state != State.trigger)
        //    activeConversation = false;
        ActiveSTT = true;


        //}

    }
    #endregion


    

    public void DebugTest(string s)
    {
        //activeConversation = true;
        SearchPanelGO.SetActive(false);
        var input_text = new MessageInput()
        {
            Text = s,
            Options = new MessageInputOptions()
            {
                ReturnContext = true
            }

        };

        _assistant.Message(OnMessageSendRequest, assistantId, sessionId, input: input_text);
    }

    public string testFind;
    void Update()
    {
        //just for debug
        if (Input.GetMouseButtonDown(1))
        {
            DebugTest(testFind);
        }
        /*
        if (Input.GetMouseButtonDown(0))
            PlayFadeIn();

        if (Input.GetMouseButtonDown(1))
            PlayFadeOut();
        */
        try
        {
            //if (!audioSource.isPlaying && !ActiveSTT)
            //{
            //    //print("salsa is not playing");
            //    ActiveSTT = true;
            //    StartRecording();
            //    // TimeDelay();
            //}
            //else
            //{
            //   // print("salsa is playing");
            //    ActiveSTT = false;
            //    StopRecording();

            //}
        }
        catch
        {
            //Log.Debug("Null exception", "Update found null exception");
        }

    }

    void TimeDelay()
    {
        seconds = seconds += Time.deltaTime;
        //print(seconds);
        //if (seconds > 20 && AnimeDone)
        //    PlayFadeOut();
    }

    public IEnumerator GetCateories(string uri)
    {
        using (UnityWebRequest webRequest = UnityWebRequest.Get(uri))
        {
            // Request and wait for the desired page.
            yield return webRequest.SendWebRequest();

            if (webRequest.isNetworkError)
            {
                Debug.Log(": Error: " + webRequest.error);
            }
            else
            {
               
                string json = "{\"details\":" + webRequest.downloadHandler.text + "}";
                Debug.Log("Received: " + json);
                Categories category = JsonConvert.DeserializeObject<Categories>(json);
                MallData.Instance.Categories = category;
                PerformAction(category);
               
            }
        }
    }

    public void PerformAction(PointOfInterest.StoreDataRoot storeDataRoot)
    {
        print(storeDataRoot.context.skills.mainskill.user_defined.webhook_result_1.result[0].store_details.location);
        ClearOptionsPanel();
        OptionsPanelGo.SetActive(false);
        activeConversation = false;
        if (!IsMapLoaded) // to make sure map is loaded only once
        {
            IsMapLoaded = true;
            cam.enabled = false;
            StartCoroutine(ActivateMap(true));
            //print("activated!");
        }
        else
        {
            print("hello yahan");
           
            foreach (var item in GameObject.FindObjectsOfType<Store>())
            {
                //print(item.ID);
                if (item.Name.ToLower() == MallData.Instance.StoreDetails.store_name.ToLower())
                {
                    //GameObject.FindObjectOfType<TargetUI>().Destination(item);
                    GameObject.FindObjectOfType<StoreInfo>().GetInfo();
                    print("Name: " + item.Name);
                }
                    
            }

        }

    }


    public void PerformAction(FindPointOfInterest.Root interest)
    {
        print(interest.context.skills.mainskill.user_defined.webhook_result_1.result[0].store_details.location);
        activeConversation = false;
        if (!IsMapLoaded) // to make sure map is loaded only once
        {
            IsMapLoaded = true;
            cam.enabled = false;
            StartCoroutine(ActivateMap(true));
            //print("activated!");
        }
        else
        {
            print("hello yahan");

            foreach (var item in GameObject.FindObjectsOfType<Store>())
            {
                //print(item.ID);
                if (item.Name == MallData.Instance.InterestDetails.store_name)
                {
                    //GameObject.FindObjectOfType<TargetUI>().Destination(item);
                    GameObject.FindObjectOfType<StoreInfo>().GetInfo();
                    print("Name: " + item.Name);
                }

            }

        }

    }


    public void PerformAction(Command.CommandData commandData)
    {
        print(commandData.output.entities[0].value);
        activeConversation = false;
        switch (commandData.output.entities[0].value)
        {
            case "home":
                if (IsMapLoaded)
                {
                    IsMapLoaded = false;
                    cam.enabled = true;
                    StartCoroutine(ActivateMap(false));
                }
                break;
            case "map":
                directMap = true;
                if (!IsMapLoaded)
                {
                    IsMapLoaded = true;
                    cam.enabled = false;
                    StartCoroutine(ActivateMap(true));

                }
                break;
            default:
                break;
        }


    }

    public void PerformAction(Options.OptionsData optionsData)
    {
        if (SceneManager.GetSceneByName("Map").isLoaded)
            return;

        Debug.Log("PerformAction(Options.OptionsData optionsData) - Message: " + optionsData.output.generic[0].text);
        activeConversation = false;
        print("Total options " + optionsData.context.skills.mainskill.user_defined.option_list.result[1].options.Count);

        ClearOptionsPanel();

        foreach (var option in optionsData.context.skills.mainskill.user_defined.option_list.result[1].options)
        {
            Debug.Log("Available Options Items: " + option.store_name);
            //instantiate option button

            OptionsPanelGo.SetActive(true);

            OptionsPanelGo.GetComponent<OptionsPanel>().Populate(option);
        }
        //switch (optionsData.output.entities[0].value.ToLower())
        //{
        //    case "fashion":
        //        print("its a options" + optionsData.context.skills.mainskill.user_defined.option_list.result[1].options.Count);

        //        ClearOptionsPanel();

        //        foreach (var option in optionsData.context.skills.mainskill.user_defined.option_list.result[1].options)
        //        {
        //            print(option.store_name);
        //            //instantiate option button

        //            OptionsPanelGo.SetActive(true);

        //            OptionsPanelGo.GetComponent<OptionsPanel>().Populate(option);
        //        }
        //        break;

        //    default:
        //        break;
        //}


    }

    public void PerformAction(Categories category)
    {
        //if (SceneManager.GetSceneByName("Map").isLoaded)
        //{
        //    ActivateMap(false);
        //}
        //activeConversation = false;
        ClearOptionsPanel();

        foreach (var item in category.details)
        {
           
            OptionsPanelGo.SetActive(true);

            OptionsPanelGo.GetComponent<OptionsPanel>().Populate(item);
        }

    }


    public void PerformAction(string data)
    {
        //if (state == State.Map)
        //    return;
        
        ClearOptionsPanel();
        OptionsPanelGo.SetActive(false);
        var input_text = new MessageInput()
        {
            Text = data,
            Options = new MessageInputOptions()
            {
                ReturnContext = true
            }

        };

        _assistant.Message(OnMessageSendRequest, assistantId, sessionId, input: input_text);
    }

    void ClearOptionsPanel()
    {
        if(!OptionsPanelGo)
        {
            Transform content = OptionsPanelGo.transform.GetChild(2).GetChild(0).Find("Content").transform;
            if (content.childCount > 0)
            {
                for (int i = 0; i < content.childCount; i++)
                {
                    Destroy(content.transform.GetChild(i).gameObject);
                    // print(OptionsPanelGo.transform.GetChild(i));
                }

            }
        }
       
    }

    public void SearchBtn()
    {
        activeConversation = false;
        if (SceneManager.GetSceneByName("Map").isLoaded)
        {
            Homebtn();
        }

        ActiveSTT = false;  //Stop Microphone from listening

        var input_text = new MessageInput()
        {
            Text = "hello",
            Options = new MessageInputOptions()
            {
                ReturnContext = true
            }

        };

        _assistant.Message(OnMessageSendRequest, assistantId, sessionId, input: input_text);


        //////////////////////////////////////////////////////////////////////////////////////////
        //if (!SceneManager.GetSceneByName("Map").isLoaded)
        //{
        //    if (state == State.Map)
        //        return;
        //    state = State.Trigger;
        //    print("Its a trigger");
        //    activeConversation = true;

        //    OptionsPanelGo.SetActive(false);
        //    SearchPanelGO.SetActive(true);

        //}


    }

    public bool MakeActiveConversation
    {
        get { return activeConversation; }
        set { activeConversation = value; }
    }

    public void Homebtn()
    {
        if (IsMapLoaded)
        {
            IsMapLoaded = false;
            cam.enabled = true;
            StartCoroutine(ActivateMap(false));
        }
    }

    public void Mapbtn()
    {
        directMap = true;
        
        if (!IsMapLoaded)
        {
            IsMapLoaded = true;
            cam.enabled = false;
            StartCoroutine(ActivateMap(true));
        }
    }

    public void InitiateCategory()
    {
        StartCoroutine(GetCateories(CategoryURL));
    }

    /*
    void PlayFadeIn()
    {
        if(!AnimeDone)
            animator.SetTrigger("FadeIn");
        AnimeDone = true;
        seconds = 0;
        print("Iam here");
    }

    void PlayFadeOut()
    {
        seconds = 0;
        AnimeDone = false;
        animator.SetTrigger("FadeOut");
       
    }*/
}
public class JsonHelper
{
    public static T[] getJsonArray<T>(string json)
    {
        string newJson = "{ \"array\": " + json + "}";
        Wrapper<T> wrapper = JsonUtility.FromJson<Wrapper<T>>(newJson);
        return wrapper.array;
    }

    [System.Serializable]
    private class Wrapper<T>
    {
        public T[] array;
    }
}