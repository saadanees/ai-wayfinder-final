﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Billboard : MonoBehaviour
{
    private void LateUpdate()
    {
        //transform.forward = new Vector3(Camera.main.transform.forward.x, transform.forward.y, Camera.main.transform.forward.z);
        if (!MapController.Instance.MapToggle)
        {
            //print(MapController.Instance.MapToggle);
            gameObject.GetComponent<SpriteRenderer>().enabled = true;
            transform.LookAt(Camera.main.transform.position, Vector3.up);
        }
        else
            gameObject.GetComponent<SpriteRenderer>().enabled = false;
    }
}
