﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;


namespace Pathfinding
{
    public class Store : MonoBehaviour
    {
        [SerializeField]
        public string ID;

        [SerializeField]
        public string Name;

        [SerializeField]
        public Transform Location;

        public bool isActive = false;

        private void OnTriggerEnter(Collider other)
        {
            //print(other.gameObject.layer);
            // layer number 8 which is Node
            if (other.gameObject.layer == 8)
            {
                Destroy(other.gameObject);
            }
        }

        private void OnCollisionEnter(Collision collision)
        {
            //print("Collision " + collision.gameObject.name);
        }

        public Vector3 CurrentLoc()
        {
            return Location.localPosition;
        }
    }



}


