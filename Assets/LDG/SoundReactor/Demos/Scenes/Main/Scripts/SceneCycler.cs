﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneCycler : MonoBehaviour
{
    public List<string> sceneNames;
    public int startIndex = 0;

    private int sceneIndex;
    private int previousIndex;
    private int index;
    private AsyncOperation unloadOperation;

    private int GetIndex(int index)
    {
        return (int)Mathf.Repeat(index, sceneNames.Count);
    }

	// Use this for initialization
	void Start ()
    {
        previousIndex = startIndex;
        sceneIndex = startIndex;

        SceneManager.LoadScene(sceneNames[sceneIndex], LoadSceneMode.Additive);
    }
	
	// Update is called once per frame
	void Update ()
    {
        bool keyPressed = false;
        bool unloading = (unloadOperation != null) ? !unloadOperation.isDone : false;

        if (!unloading)
        {
            if (Input.GetKeyDown(KeyCode.DownArrow))
            {
                previousIndex = GetIndex(index);
                index--;
                sceneIndex = GetIndex(index);
                keyPressed = true;
            }

            if (Input.GetKeyDown(KeyCode.UpArrow))
            {
                previousIndex = GetIndex(index);
                index++;
                sceneIndex = GetIndex(index);
                keyPressed = true;
            }
        }

        if (keyPressed)
        {
            Debug.Log(sceneIndex);

            Scene scene = SceneManager.GetSceneByName(sceneNames[previousIndex]);

            if (scene.isLoaded)
            {
                unloadOperation = SceneManager.UnloadSceneAsync(sceneNames[previousIndex]);
                SceneManager.LoadScene(sceneNames[sceneIndex], LoadSceneMode.Additive);
            }
        }
    }
}
