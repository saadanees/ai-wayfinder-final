﻿using UnityEngine;
using LDG.SoundReactor;

public class EmitObjectHandler : MonoBehaviour
{
    public GameObject spawnObject;
    public Color color = Color.blue;

    public void OnLevel(PropertyDriver driver)
    {
        Renderer renderer;
        GameObject go;

        if(spawnObject != null && driver.isBeat)
        {
            go = Instantiate(spawnObject, this.transform.position, this.transform.rotation);
            go.transform.SetParent(transform);

            renderer = go.GetComponent<Renderer>();
            renderer.material.SetColor("_Color", this.color);
            renderer.material.SetColor("_EmissionColor", this.color);
        }
    }
}
