﻿using UnityEngine;

public class TranslateLocalMotion : MonoBehaviour
{
    public Vector3 direction = Vector3.forward;
    public float rate = 1.0f;
	
	// Update is called once per frame
	void Update ()
    {
        Vector3 pos = transform.localPosition;

        transform.localPosition = pos + (direction * rate * Time.deltaTime);
	}
}
