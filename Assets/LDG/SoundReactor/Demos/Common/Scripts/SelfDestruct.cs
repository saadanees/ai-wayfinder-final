﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SelfDestruct : MonoBehaviour
{
    public float timer = 1.0f;
    public GameObject spawnObject;
    public Transform spawnParent;

    private float counter = 0.0f;

	// Use this for initialization
	void Start ()
    {
        counter = 0.0f;
	}
	
	// Update is called once per frame
	void Update ()
    {
        counter += Time.deltaTime;

        if(counter > timer)
        {
            if(spawnObject)
            {
                Instantiate(spawnObject, transform.position, spawnObject.transform.rotation, spawnParent);
            }

            Destroy(gameObject);
        }
	}
}
