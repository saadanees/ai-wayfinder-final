﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.AI;

public class NavMeshLine : MonoBehaviour
{
    public Transform NikeStore;

    Rigidbody rb;
    NavMeshAgent agent;
    RaycastHit hit;
    public LineRenderer line;
    public Vector3[] points;
    Vector3 currentPos;
    bool isMoving;

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        agent = GetComponent<NavMeshAgent>();
        currentPos = this.transform.position;
        isMoving = false;

       // line = GetComponent<LineRenderer>();
    }

    // Update is called once per frame
    void Update()
    {
        //if (Input.GetMouseButton(0))
        //{

        //    Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        //    if (Physics.Raycast(ray, out hit))
        //    {
        //        if (hit.collider.tag == "Clickable")
        //        {
        //            agent.transform.position = new Vector3(currentPos.position.x, currentPos.position.y, currentPos.position.z);
        //            agent.Warp(currentPos);
        //            agent.SetDestination(hit.collider.transform.position);
        //            print(hit.collider.transform.position);
        //        }
        //        agent.SetDestination(hit.point);
        //    }
        //    DeplayPathWay();
        //}

        // agent.SetDestination(NikeStore.position);

        if (isMoving)
        {
            agent.SetDestination(NikeStore.position);
            DeplayPathWay();
            isMoving = false;
           
            // OnDrawLine();

        }

        if (agent.isStopped)
        {
            isMoving = false;
            agent.Warp(currentPos);
        }



    }

    void OnDrawGizmos()
    {

       // var nav = agent;
       // if (nav == null || nav.path == null)
       //     return;

       //// var line = this.GetComponent<LineRenderer>();
       // if (line == null)
       // {
       //     line = this.gameObject.AddComponent<LineRenderer>();
       //     line.material = new Material(Shader.Find("Sprites/Default")) { color = Color.yellow };
       //     line.startWidth = 0.5f;
       //     line.startColor = Color.red;
       // }

       // var path = nav.path;
        
       // line.positionCount = path.corners.Length;

       // for (int i = 0; i < path.corners.Length; i++)
       // {
       //     line.SetPosition(i, path.corners[i]);
       // }

    }

    public void Destination(int num)
    {
        switch (num)
        {
            case 001:
                print("Nike store");
                agent.SetDestination(NikeStore.position);
                isMoving = true;

               
                
                break;
            default:
                break;
        }
      
    }

    void DeplayPathWay()
    {
        //if (agent.path.corners.Length < 2)
        //    return;

        points = new Vector3[agent.path.corners.Length];
        line.positionCount = points.Length;
        line.startWidth = 1f;
        line.endWidth = 1f;

        for (int i = 0; i < points.Length; i++)
        {
            Vector3 checkpointpos = agent.path.corners[i];
            points[i] = new Vector3(checkpointpos.x, checkpointpos.y, checkpointpos.z);
        }

        //line.SetPositions(agent.path.corners);
        line.SetPositions(points);

        
    }

}
