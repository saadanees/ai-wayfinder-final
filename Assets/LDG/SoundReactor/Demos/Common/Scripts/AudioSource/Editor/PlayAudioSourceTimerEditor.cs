﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace LDG.SoundReactor.Midi
{
    [CustomEditor(typeof(PlayAudioSourceTimer))]
    public class PlayAudioSourceTimerEditor : Editor
    {
        public override void OnInspectorGUI()
        {
            this.DrawDefaultInspector();

            PlayAudioSourceTimer source = (PlayAudioSourceTimer)target;

            EditorGUILayout.BeginHorizontal();

            if (GUILayout.Button("Play"))
            {
                source.Play();
                //Debug.Log("Play");
            }

            if (GUILayout.Button("Pause"))
            {
                source.Pause();
                //Debug.Log("Pause");
            }

            if (GUILayout.Button("Stop"))
            {
                source.Stop();
                //Debug.Log("Stop");
            }

            EditorGUILayout.EndHorizontal();
        }
    }
}