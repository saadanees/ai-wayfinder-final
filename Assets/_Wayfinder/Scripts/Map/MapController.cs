﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Pathfinding;
using System;
using DG.Tweening;

public class MapController : Singleton<MapController>
{
    public GameObject StoreInfoPenalGO;

    public Camera mainCam;
    Vector3 camTransform;

    private bool mapToggle;

    public bool MapToggle { get => mapToggle; set => mapToggle = value; }

    protected override void Awake()
    {
        base.Awake();
        camTransform = new Vector3(mainCam.transform.position.x, mainCam.transform.position.y, mainCam.transform.position.z);
        //print(camTransform);
    if(TheBrainScript.Instance != null)
        {
            if (TheBrainScript.Instance.directMap)
                StoreInfoPenalGO.SetActive(false);
        }
    }
    
    public void OpenInfo()
    {
        StoreInfoPenalGO.SetActive(true);
    }

    public void Btn_Toggle()
    {
        if (!MapToggle)
            MapToggle = true;
        else
            MapToggle = false;
        StartCoroutine(SwitchTopView(mapToggle));
    }

    private IEnumerator SwitchTopView(bool isOn)
    {
        Sequence mySequence = DOTween.Sequence();
        if (isOn)
        {
            // top view
            mainCam.orthographic = false;
            
            mySequence.Append(mainCam.transform.DOMove(new Vector3(12.6f, 51f, -27f), 1))
              .Join(mainCam.transform.DORotate(new Vector3(90f, -90f, 0), 1))
              .Join(mainCam.DOFieldOfView(112, 1));

         

            //mainCam.transform.position = new Vector3(12.6f, 51f, -27f);
            //mainCam.transform.rotation = Quaternion.Euler(90f,-90f,0);
            
            //mainCam.fieldOfView = 112f;

        }
        else
        {
            // isometric
            //(50.0, 51.0, -58.0)
            // (45f, -45f, 0);
            mainCam.orthographic = true;
            mySequence.Append(mainCam.transform.DOMove(new Vector3(camTransform.x, camTransform.y, camTransform.z), 1))
              .Join(mainCam.transform.DORotate(new Vector3(45f, -45f, 0), 1))
              .Join(mainCam.DOOrthoSize(65f, 1f));
            //print(camTransform);
            //mainCam.transform.position = new Vector3(camTransform.x, camTransform.y, camTransform.z);
            //mainCam.transform.rotation = Quaternion.Euler(45f, -45f, 0);
          
            //mainCam.fieldOfView = 65f;
        }

        yield return new WaitForSeconds(0f);

    }

    private void OnEnable()
    {
       
        if (TheBrainScript.Instance != null)
        {
            if (TheBrainScript.Instance.directMap)
                StoreInfoPenalGO.SetActive(false);
        }

    }
}
