﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Pathfinding
{
    public class DrawLine : VersionedMonoBehaviour
    {
        public LineRenderer lineRenderer;
        Vector3 pointA, pointB;
        float counter, dist;
        // Start is called before the first frame update
        void Start()
        {
            lineRenderer.positionCount = 0;
            counter = 0;
            dist = 0;
        }

        // Update is called once per frame
        void Update()
        {
            if (counter < dist)
            {
               
                counter += 0.1f;
                float x = Mathf.Lerp(0, dist, counter);

                Vector3 a = pointA;
                Vector3 b = pointB;

                Vector3 line = x * Vector3.Normalize(pointB - pointB) + pointA;

                lineRenderer.SetPosition(1, line);

                print(counter < dist);
            }
        }

        public void DrawPath(List<Vector3> pathWay)
        {
            
            lineRenderer.positionCount = 0;

            //print("drawing line " + pathWay.Count);
            Vector3[] points = new Vector3[pathWay.Count];


            for (int i = 0; i < pathWay.Count; i++)
            {
                Vector3 checkpointpos = pathWay[i];
                //  print(checkpointpos);
                points[i] = new Vector3(checkpointpos.x, checkpointpos.y, checkpointpos.z);
            }

            pointA = points[0];
            pointB = points[points.Length - 1];
            dist = Vector3.Distance(pointA, pointB);

            lineRenderer.startWidth = 2f;
            lineRenderer.endWidth = 2f;
            lineRenderer.positionCount = pathWay.Count;
            //lineRenderer.SetPositions(points);

        }
    }
}
